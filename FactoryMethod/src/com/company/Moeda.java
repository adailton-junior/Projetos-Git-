
// Interface Moeda

package com.company;

public interface Moeda {

    public String simboloMoeda();
    public double getValor();
    public String infoMoeda();
}
