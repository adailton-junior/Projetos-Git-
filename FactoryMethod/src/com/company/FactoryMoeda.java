
// FactoryMoeda
package com.company;

public  class FactoryMoeda {

    public static Moeda criaMoeda(String pais, double valor){
        if(pais=="Brasil"){
            return new Real(valor);
        }else if(pais=="Estados Unidos"){
            return new Dolar(valor);
        }
        throw new IllegalArgumentException("Pais Inexistente");
    }
}
