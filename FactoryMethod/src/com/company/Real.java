
// Classe Real
package com.company;

public class Real implements Moeda {

    double valor;

    Real(double valor){
        this.valor=valor;
    }

    @Override
    public String simboloMoeda() {
        return "R$";
    }

    @Override
    public double getValor() {
        return valor;
    }
    @Override
    public String infoMoeda() {
        return simboloMoeda() + " " +getValor();
    }
}
