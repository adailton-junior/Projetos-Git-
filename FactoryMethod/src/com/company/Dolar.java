
// Classe Dolar
package com.company;

public class Dolar implements Moeda {

    double valor;

    Dolar(double valor){
        this.valor=valor;
    }


    @Override
    public String simboloMoeda() {
        return "USD";
    }

    @Override
    public double getValor() {
        return valor;
    }

    @Override
    public String infoMoeda() {
        return simboloMoeda() +" " + getValor();
    }
}
