
//Classe Main
package com.company;

public class Main {

    public static void main(String[] args) {
	// write your code here

        Moeda m1 = FactoryMoeda.criaMoeda("Brasil", 0.50);
        Moeda m2 = FactoryMoeda.criaMoeda("Estados Unidos", 1);

        System.out.println(m1.infoMoeda());
        System.out.println(m2.infoMoeda());

    }
}
